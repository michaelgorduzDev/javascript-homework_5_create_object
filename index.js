function createNewUser() {

    // adding firstname and lastname to an object 
    const newUser = {
        _firstName: prompt("Enter your name"),
        _lastName: prompt("Enter your last name"),
        get fullName() {
            return `${this._firstName} ${this._lastName}`;
        }
    };

    newUser.setFirstName = function (firstName) {
        this._firstName = firstName;
    };

    newUser.setLastName = function (lastName) {
        this._lastName = lastName;
    };

    //   adding getLogin function 
    newUser.getLogin = function () {
        return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
    };

    return newUser;
}


const user = createNewUser();

// user.setFirstName("Bruce");
// user.setLastName("Lee");
console.log("You entered: " + user.fullName);
console.log("Your suggested login is: " + user.getLogin());